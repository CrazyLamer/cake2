<?php
App::uses('AppController', 'Controller');

/**
 * Tasks Controller
 *
 * @property Task $Task
 * @property PaginatorComponent $Paginator
 */
class TasksController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Paginator'];


    public function isAuthorized($user)
    {
        // All registered users can add posts
        if (in_array($this->action, ['add', 'index', 'view', ''])) {
            return true;
        }

        // The owner of a post can edit and delete it
        if (in_array($this->action, ['edit', 'delete'])) {
            $taskId = (int)$this->request->params['pass'][0];
            if ($this->Task->isOwnedBy($taskId, $user['id'])) {
                return true;
            }
        }
        if (in_array($this->action, ['edit'])) {
            $taskId = (int)$this->request->params['pass'][0];
            if ($this->Task->isExecutorBy($taskId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }


    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->Task->recursive = 0;
        $this->set('tasks', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->Task->exists($id)) {
            throw new NotFoundException(__('Invalid task'));
        }
        $options = ['conditions' => ['Task.' . $this->Task->primaryKey => $id]];
        $this->set('task', $this->Task->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->request->data['Task']['user_id'] = $this->Auth->user('id');
            $this->Task->create();
            if ($this->Task->save($this->request->data)) {
                $this->Flash->success(__('The task has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The task could not be saved. Please, try again.'));
            }
        }
        $types = $this->Task->Type->find('list');
        $executors = $this->Task->Executor->find('list', ['fields' => [
            'Executor.id',
            'Executor.username',
        ]]);
        $statuses = $this->Task->Status->find('list');
        $this->set(compact('types', 'users', 'executors', 'statuses'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->Task->exists($id)) {
            throw new NotFoundException(__('Invalid task'));
        }
        if ($this->request->is(['post', 'put'])) {
            if ($this->Task->save($this->request->data)) {
                $this->Flash->success(__('The task has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The task could not be saved. Please, try again.'));
            }
        } else {
            $options = ['conditions' => ['Task.' . $this->Task->primaryKey => $id]];
            $this->request->data = $this->Task->find('first', $options);
        }
        $types = $this->Task->Type->find('list');
        $users = $this->Task->User->find('list', ['fields' => [
            'User.id',
            'User.username',
        ]]);
        $executors = $this->Task->Executor->find('list', ['fields' => [
            'Executor.id',
            'Executor.username',
        ]]);
        $statuses = $this->Task->Status->find('list');
        $this->set(compact('types', 'users', 'executors', 'statuses'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->Task->id = $id;
        if (!$this->Task->exists()) {
            throw new NotFoundException(__('Invalid task'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Task->delete()) {
            $this->Flash->success(__('The task has been deleted.'));
        } else {
            $this->Flash->error(__('The task could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
