-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.14-MariaDB - mariadb.org binary distribution
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица cake2.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake2.statuses: ~4 rows (приблизительно)
DELETE FROM `statuses`;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `name`) VALUES
	(2, 'В работе'),
	(3, 'Выполнена'),
	(5, 'Отменена'),
	(1, 'Создана');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;


-- Дамп структуры для таблица cake2.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `comment` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `executor_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type_id`),
  KEY `author` (`user_id`),
  KEY `responsible_employee` (`executor_id`),
  KEY `status` (`status_id`),
  CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tasks_ibfk_3` FOREIGN KEY (`executor_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tasks_ibfk_4` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake2.tasks: ~23 rows (приблизительно)
DELETE FROM `tasks`;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`id`, `name`, `description`, `comment`, `type_id`, `user_id`, `executor_id`, `status_id`, `created`, `modified`) VALUES
	(1, 'Написать парсер', 'Парсер маркет', 'Хз', 1, 1, 1, 1, '2016-11-28 12:05:44', '2016-11-28 12:05:44'),
	(2, 'Написать выгрузку', 'Написать выгрузку товаров', '', 1, 1, 1, 1, '2016-11-30 07:54:22', '2016-11-30 07:54:22'),
	(3, 'Меню', 'Добавить новый пункт меню', 'qwee', 2, 2, 1, 5, '2016-11-30 07:55:55', '2016-12-01 11:35:56'),
	(12, 'qewqeq', 'qweqweqweqwe', 'qweqweqweqweqwe', 3, 1, 4, 2, '2016-12-01 11:57:10', '2016-12-01 11:58:16'),
	(24, 'daesfsdrfdsf', 'sdfdsfdsfdsf', 'QQQQQQQQQ', 3, 1, 2, 2, '2016-12-01 12:15:18', '2016-12-01 13:58:10'),
	(27, 'qqqq', 'wwwwww', 'eeeeeeee', 1, 2, 4, 1, '2016-12-02 06:43:24', '2016-12-02 06:43:24'),
	(28, 'test2', 'qweqweqwe', 'qweqwerewrw', 2, 1, NULL, 2, '2016-12-02 13:00:05', '2016-12-02 13:00:05'),
	(29, 'test2', 'qweqweqwe', 'qweqwerewrw', 2, 1, 4, 2, '2016-12-02 13:02:15', '2016-12-02 13:02:15'),
	(30, 'test2', 'qweqweqwe', 'qweqwerewrw', 2, 1, 4, 2, '2016-12-02 13:02:54', '2016-12-02 13:02:54'),
	(31, 'test2', 'qweqweqwe', 'qweqwerewrw', 2, 1, 4, 2, '2016-12-02 13:04:00', '2016-12-02 13:04:00'),
	(32, 'test2', 'qweqweqwe', 'qweqwerewrw', 2, 1, NULL, 2, '2016-12-02 13:04:16', '2016-12-02 13:04:16'),
	(33, 'test2', 'qweqweqwe', 'qweqwerewrw', 2, 1, NULL, 2, '2016-12-02 13:04:55', '2016-12-02 13:04:55'),
	(34, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 2, '2016-12-02 13:09:26', '2016-12-02 13:09:26'),
	(35, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 2, '2016-12-02 13:10:30', '2016-12-02 13:10:30'),
	(36, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 5, '2016-12-02 13:10:41', '2016-12-02 13:10:41'),
	(37, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 5, '2016-12-02 13:12:58', '2016-12-02 13:12:58'),
	(38, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 5, '2016-12-02 13:13:23', '2016-12-02 13:13:23'),
	(39, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 5, '2016-12-02 13:15:33', '2016-12-02 13:15:33'),
	(40, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 5, '2016-12-02 13:15:56', '2016-12-02 13:15:56'),
	(46, 'dawd', 'qawdeqaw', 'qweqweqwDDDDDD', 3, 1, 4, 5, '2016-12-02 13:24:27', '2016-12-02 17:08:27'),
	(47, 'dawd', 'qawdeqaw', 'qweqweqwQQQQ', 3, 1, 4, 5, '2016-12-02 13:26:30', '2016-12-02 16:19:52'),
	(48, 'dawd', 'qawdeqaw', 'qweqweqw', 3, 1, 4, 5, '2016-12-02 13:30:03', '2016-12-02 13:30:03'),
	(49, 'dqawdreewfrwe', 'rwerwerwe', 'rwerwerwer', 3, 1, NULL, 2, '2016-12-02 13:46:10', '2016-12-02 13:46:10'),
	(54, 'QQQQQ', 'WWWW', 'EERRRRRRR', 1, 1, NULL, 1, '2016-12-02 15:32:00', '2016-12-02 15:32:00');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;


-- Дамп структуры для таблица cake2.types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake2.types: ~3 rows (приблизительно)
DELETE FROM `types`;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `name`) VALUES
	(3, 'Незначительное улучшение'),
	(2, 'Несрочный баг'),
	(1, 'Срочный баг');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;


-- Дамп структуры для таблица cake2.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL DEFAULT 'user',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake2.users: ~4 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `description`, `role`, `created`, `modified`) VALUES
	(1, 'admin', '$2a$10$7xpdg.kP2AOzrWiFzaoOhuV43tLnXXZ6zcIvjEdZJ508a.bf8l.hS', 'root', 'admin', '2016-11-28 13:40:30', '2016-11-30 16:23:37'),
	(2, 'superuser', '$2a$10$7xpdg.kP2AOzrWiFzaoOhuV43tLnXXZ6zcIvjEdZJ508a.bf8l.hS', 'Главный программист', 'user', '2016-11-28 13:40:30', '2016-12-02 14:59:52'),
	(3, 'user', '$2a$10$7xpdg.kP2AOzrWiFzaoOhuV43tLnXXZ6zcIvjEdZJ508a.bf8l.hS', 'Программист', 'user', '2016-11-28 13:40:30', '2016-11-28 13:40:30'),
	(4, 'user2', '$2a$10$7xpdg.kP2AOzrWiFzaoOhuV43tLnXXZ6zcIvjEdZJ508a.bf8l.hS', 'Программист', 'user', '2016-11-28 13:40:30', '2016-12-01 10:23:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
