<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('description');
        echo $this->Form->input('role', [
            'options' => ['admin' => 'Admin', 'user' => 'User'],
            'value' => 'user',
        ]);
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('List Users'), ['action' => 'index']); ?></li>
        <li><?php echo $this->Html->link(__('List Tasks'), ['controller' => 'tasks', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Task'), ['controller' => 'tasks', 'action' => 'add']); ?> </li>
    </ul>
</div>
