<div class="users view">
    <h2><?php echo __('User'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($user['User']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Username'); ?></dt>
        <dd>
            <?php echo h($user['User']['username']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Password'); ?></dt>
        <dd>
            <?php echo h($user['User']['password']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Description'); ?></dt>
        <dd>
            <?php echo h($user['User']['description']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Role'); ?></dt>
        <dd>
            <?php echo h($user['User']['role']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($user['User']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
            <?php echo h($user['User']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit User'), ['action' => 'edit', $user['User']['id']]); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user['User']['id']], ['confirm' => __('Are you sure you want to delete # %s?', $user['User']['id'])]); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), ['action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), ['action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Tasks'), ['controller' => 'tasks', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Task'), ['controller' => 'tasks', 'action' => 'add']); ?> </li>
    </ul>
</div>
<div class="related">
    <h3><?php echo __('Related Tasks'); ?></h3>
    <?php if (!empty($user['Task'])): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Description'); ?></th>
                <th><?php echo __('Comment'); ?></th>
                <th><?php echo __('Type Id'); ?></th>
                <th><?php echo __('User Id'); ?></th>
                <th><?php echo __('Executor Id'); ?></th>
                <th><?php echo __('Status Id'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <th><?php echo __('Modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($user['Task'] as $task): ?>
                <tr>
                    <td><?php echo $task['id']; ?></td>
                    <td><?php echo $task['name']; ?></td>
                    <td><?php echo $task['description']; ?></td>
                    <td><?php echo $task['comment']; ?></td>
                    <td><?php echo $task['type_id']; ?></td>
                    <td><?php echo $task['user_id']; ?></td>
                    <td><?php echo $task['executor_id']; ?></td>
                    <td><?php echo $task['status_id']; ?></td>
                    <td><?php echo $task['created']; ?></td>
                    <td><?php echo $task['modified']; ?></td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), ['controller' => 'tasks', 'action' => 'view', $task['id']]); ?>
                        <?php echo $this->Html->link(__('Edit'), ['controller' => 'tasks', 'action' => 'edit', $task['id']]); ?>
                        <?php echo $this->Form->postLink(__('Delete'), ['controller' => 'tasks', 'action' => 'delete', $task['id']], ['confirm' => __('Are you sure you want to delete # %s?', $task['id'])]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>

    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('New Task'), ['controller' => 'tasks', 'action' => 'add']); ?> </li>
        </ul>
    </div>
</div>
