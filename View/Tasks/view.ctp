<div class="tasks view">
    <h2><?php echo __('Task'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($task['Task']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($task['Task']['name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Description'); ?></dt>
        <dd>
            <?php echo h($task['Task']['description']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Comment'); ?></dt>
        <dd>
            <?php echo h($task['Task']['comment']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Type'); ?></dt>
        <dd>
            <?php echo $this->Html->link($task['Type']['name'], ['controller' => 'types', 'action' => 'view', $task['Type']['id']]); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('User'); ?></dt>
        <dd>
            <?php echo $this->Html->link($task['User']['username'], ['controller' => 'users', 'action' => 'view', $task['User']['id']]); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Executor'); ?></dt>
        <dd>
            <?php echo $this->Html->link($task['Executor']['username'], ['controller' => 'users', 'action' => 'view', $task['Executor']['id']]); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
            <?php echo $this->Html->link($task['Status']['name'], ['controller' => 'statuses', 'action' => 'view', $task['Status']['id']]); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($task['Task']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
            <?php echo h($task['Task']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Task'), ['action' => 'edit', $task['Task']['id']]); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Task'), ['action' => 'delete', $task['Task']['id']], ['confirm' => __('Are you sure you want to delete # %s?', $task['Task']['id'])]); ?> </li>
        <li><?php echo $this->Html->link(__('List Tasks'), ['action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Task'), ['action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Types'), ['controller' => 'types', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Type'), ['controller' => 'types', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), ['controller' => 'users', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), ['controller' => 'users', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Statuses'), ['controller' => 'statuses', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Status'), ['controller' => 'statuses', 'action' => 'add']); ?> </li>
    </ul>
</div>
