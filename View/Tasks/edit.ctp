<div class="tasks form">
    <?php echo $this->Form->create('Task'); ?>
    <fieldset>
        <legend><?php echo __('Edit Task'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('name');
        echo $this->Form->input('description');
        echo $this->Form->input('comment');
        echo $this->Form->input('type_id');
        echo $this->Form->input('user_id');
        echo $this->Form->input('executor_id', ['empty' => true]);
        echo $this->Form->input('status_id');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $this->Form->value('Task.id')], ['confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Task.id'))]); ?></li>
        <li><?php echo $this->Html->link(__('List Tasks'), ['action' => 'index']); ?></li>
        <li><?php echo $this->Html->link(__('List Types'), ['controller' => 'types', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Type'), ['controller' => 'types', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), ['controller' => 'users', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), ['controller' => 'users', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Statuses'), ['controller' => 'statuses', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Status'), ['controller' => 'statuses', 'action' => 'add']); ?> </li>
    </ul>
</div>
