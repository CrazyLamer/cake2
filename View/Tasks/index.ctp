<div class="tasks index">
    <h2><?php echo __('Tasks'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('description'); ?></th>
            <th><?php echo $this->Paginator->sort('comment'); ?></th>
            <th><?php echo $this->Paginator->sort('type_id'); ?></th>
            <th><?php echo $this->Paginator->sort('user_id'); ?></th>
            <th><?php echo $this->Paginator->sort('executor_id'); ?></th>
            <th><?php echo $this->Paginator->sort('status_id'); ?></th>
            <th><?php echo $this->Paginator->sort('created'); ?></th>
            <th><?php echo $this->Paginator->sort('modified'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tasks as $task): ?>
            <tr>
                <td><?php echo h($task['Task']['id']); ?>&nbsp;</td>
                <td><?php echo h($task['Task']['name']); ?>&nbsp;</td>
                <td><?php echo h($task['Task']['description']); ?>&nbsp;</td>
                <td><?php echo h($task['Task']['comment']); ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($task['Type']['name'], ['controller' => 'types', 'action' => 'view', $task['Type']['id']]); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($task['User']['username'], ['controller' => 'users', 'action' => 'view', $task['User']['id']]); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($task['Executor']['username'], ['controller' => 'users', 'action' => 'view', $task['Executor']['id']]); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($task['Status']['name'], ['controller' => 'statuses', 'action' => 'view', $task['Status']['id']]); ?>
                </td>
                <td><?php echo h($task['Task']['created']); ?>&nbsp;</td>
                <td><?php echo h($task['Task']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), ['action' => 'view', $task['Task']['id']]); ?>
                    <?php echo $this->Html->link(__('Edit'), ['action' => 'edit', $task['Task']['id']]); ?>
                    <?php echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $task['Task']['id']], ['confirm' => __('Are you sure you want to delete # %s?', $task['Task']['id'])]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter([
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ]);
        ?>    </p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), [], null, ['class' => 'prev disabled']);
        echo $this->Paginator->numbers(['separator' => '']);
        echo $this->Paginator->next(__('next') . ' >', [], null, ['class' => 'next disabled']);
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Task'), ['action' => 'add']); ?></li>
        <li><?php echo $this->Html->link(__('List Types'), ['controller' => 'types', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Type'), ['controller' => 'types', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), ['controller' => 'users', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), ['controller' => 'users', 'action' => 'add']); ?> </li>
        <li><?php echo $this->Html->link(__('List Statuses'), ['controller' => 'statuses', 'action' => 'index']); ?> </li>
        <li><?php echo $this->Html->link(__('New Status'), ['controller' => 'statuses', 'action' => 'add']); ?> </li>
    </ul>
</div>
