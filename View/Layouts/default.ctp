<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css('cake.generic');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
</head>
<body>
<div id="container">
    <div id="header">
        <nav class="top-bar expanded" data-topbar role="navigation">
            <div class="top-bar-section">
                <a href="/tasks">Задания</a>
                <a href="/users">Пользователи</a>
                <a href="/users/logout">Выход</a>
            </div>
        </nav>
    </div>
    <div id="content">

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>
    </div>
    <div id="footer">
        <?php echo $this->Html->link(
            $this->Html->image('cake.power.gif', ['alt' => $cakeDescription, 'border' => '0']),
            'http://www.cakephp.org/',
            ['target' => '_blank', 'escape' => false]
        );
        ?>
    </div>
</div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
