<?php
App::uses('TypesController', 'Controller');

/**
 * TypesController Test Case
 */
class TypesControllerTest extends ControllerTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.type',
        'app.task',
        'app.user',
        'app.executor',
        'app.status'
    ];

    /**
     * testIndex method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('testIndex not implemented.');
    }

    /**
     * testView method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('testView not implemented.');
    }

    /**
     * testAdd method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('testAdd not implemented.');
    }

    /**
     * testEdit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('testEdit not implemented.');
    }

    /**
     * testDelete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('testDelete not implemented.');
    }

}
