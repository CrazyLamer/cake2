<?php
App::uses('Type', 'Model');

/**
 * Type Test Case
 */
class TypeTest extends CakeTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.type',
        'app.task',
        'app.user',
        'app.executor',
        'app.status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Type = ClassRegistry::init('Type');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Type);

        parent::tearDown();
    }

}
